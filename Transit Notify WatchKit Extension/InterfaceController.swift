//
//  InterfaceController.swift
//  Transit Notify WatchKit Extension
//
//  Created by santosh tekulapally on 2019-11-21.
//  Copyright © 2019 santosh tekulapally. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController,XMLParserDelegate {
    @IBOutlet weak var tbl_bus: WKInterfaceTable!
    var parser = XMLParser()
    var posts = NSMutableArray()
    var elements = NSMutableDictionary()
    var element = NSString()
    var title1 = [String]()

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        self.loadData()
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    private func loadData(){
        posts = []
        parser = XMLParser(contentsOf:(NSURL(string:"http://webservices.nextbus.com/service/publicXMLFeed?command=routeList&a=ttc"))! as URL)!
        parser.delegate = self
        parser.parse()
  //      tbl_bus!.reloadData()

        
    }
    private func loadTbl(){
        let name = UserDefaults.standard.string(forKey: "name") ?? ""
        print("Name",name)
        tbl_bus.setNumberOfRows(self.title1.count, withRowType:"RowController")
        for (index,rowModel) in title1.enumerated() {
            if let rowController = tbl_bus.rowController(at: index) as? RowController{
                rowController.lbl_bus.setText(title1[index])
            }
        }
    }
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String])
    {
        element = elementName as NSString
        print(element)
        if elementName == "route" {
            //let tempTag = Tag();
            if let name = attributeDict["title"] {
                self.title1.append(name)
//                title2.append(name)
//                titleTagDict.setValue(name, forKey: "title")
              //  self.tbl_nearBy.reloadData()
            }
        }
        
    }
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        pushController(withName: "MapVC", context: title1[rowIndex])
    }
    func parserDidEndDocument(_ parser: XMLParser) {
        DispatchQueue.main.async {
            self.loadTbl()
        }
    }
}
