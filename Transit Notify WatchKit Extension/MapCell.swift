//
//  MapCell.swift
//  Transit Notify WatchKit Extension
//
//  Created by santosh tekulapally on 2019-11-21.
//  Copyright © 2019 santosh tekulapally. All rights reserved.
//

import Foundation
import WatchKit
class MapCell:NSObject{
    @IBOutlet weak var lbl_bus: WKInterfaceLabel!
    
    @IBOutlet weak var lbl_prediction: WKInterfaceLabel!
    
}
