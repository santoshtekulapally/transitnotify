//
//  MapVC.swift
//  Transit Notify WatchKit Extension
//
//  Created by santosh tekulapally on 2019-11-21.
//  Copyright © 2019 santosh tekulapally. All rights reserved.
//

import Foundation
import WatchKit
class MapVC: WKInterfaceController,XMLParserDelegate{
    
    @IBOutlet weak var tbl_map2: WKInterfaceTable!
    @IBOutlet weak var tbl_map: WKInterfaceTable!
    var parser = XMLParser()
    var posts = NSMutableArray()
    var elements = NSMutableDictionary()
    var element = NSString()
    var title1 = [String]()
    var title2 = [String]()
    var minString = [String]()
    var titleBusStop = [String]()
    var tagString = ""
    var tagArray = [String]()
    var busLat = [String]()
    var locations = [CLLocation]()
    var closestLocation: CLLocation?
    var smallestDistance: CLLocationDistance?
    var busLat1 = [String]()
    var busLon1 = [String]()
    var minutesarray = [String]()

    var busLon = [String]()

    var serviceTag = "2"
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        if let str = context  as? String{
            self.tagString = str
        }
        self.beginParsing()
        self.loadData()
    }
    override func willActivate() {
        super.willActivate()
    }
    override func didDeactivate() {
        super.didDeactivate()
    }
    private func nearByBusStop(){
        
                let currentLocationCal = CLLocation(latitude:Double(self.busLat[0]) as! CLLocationDegrees, longitude:Double(self.busLon[0]) as! CLLocationDegrees)
//        print("Current location",currentLocationCal)
//                for (index,location) in locations.enumerated() {
//                    let distance = currentLocationCal.distance(from: location)
//                    print("Distance:",distance)
//                    if Int(distance) == 0 || Int(distance) < 1000 {
//                        closestLocation = location
//                        arrIndex.append(index)
//                        //smallestDistance = distance
//                        print("Index Location Array",self.arrIndex)
//
//                    }
//                }
        for (index,location) in self.busLat.enumerated()
        {
            let locationCl = CLLocation(latitude:Double(self.busLat[index]) as! CLLocationDegrees, longitude:Double(self.busLon[index]) as! CLLocationDegrees )
            self.locations.append(locationCl)
            print("Location Array",self.locations)
            let distance = currentLocationCal.distance(from: locationCl)
            print("Distance:",distance)
            if Int(distance) == 0 || Int(distance) < 100 {
                //                closestLocation = locationCl
                //                arrIndex.append(index)
                self.title2.append(title1[index])
                
                print("nearest bus stop is ",self.title2)
                //                self.filterTag.append(tagArray[index])
                //smallestDistance = distance
//                print("Index Location Array",self.arrIndex)
                //  self.tbl_nearBy.reloadData()
                self.busLat1.append(self.busLat[index])
                self.busLon1.append(self.busLon[index])
                print("Lat1 print",self.busLon1)
                print("long print",self.busLat1)

            }
            
        }
         self.loadTbl()
    }
   private func beginParsing()
        
    {
        self.serviceTag = "1"
        let concatString = ""
        let fullNameArr = self.tagString.components(separatedBy: "-")
        
        posts = []
        parser = XMLParser(contentsOf:(NSURL(string:"http://webservices.nextbus.com/service/publicXMLFeed?command=routeConfig&a=ttc&r=\(fullNameArr[0])"))! as URL)!
        //        print("http://webservices.nextbus.com/service/publicXMLFeed?command=routeConfig&a=ttc&r=\(self.tagString)")
        parser.delegate = self
        parser.parse()
        // tbl_nearBy!.reloadData()
    }
    private func loadData(){
        let fullNameArr1 = self.tagString.components(separatedBy: "-")
        self.serviceTag = "2"
        posts = []
        parser = XMLParser(contentsOf:(NSURL(string:"http://webservices.nextbus.com/service/publicXMLFeed?command=predictions&&a=ttc&r=\(fullNameArr1[0])&s=\(self.tagArray[5])&useShortTitles=true"))! as URL)!
        //        print("http://webservices.nextbus.com/service/publicXMLFeed?command=routeConfig&a=ttc&r=\(self.tagString)")
        print("http://webservices.nextbus.com/service/publicXMLFeed?command=predictions&&a=ttc&r=\(fullNameArr1[0])&s=\(self.tagArray[5])&useShortTitles=true")
        parser.delegate = self
        parser.parse()        //      tbl_bus!.reloadData()
    }
    private func loadTbl(){
        let name = UserDefaults.standard.string(forKey: "name") ?? ""
        print("Name",name)
        
        tbl_map.setNumberOfRows(self.title2.count, withRowType:"MapCell")
        tbl_map2.setNumberOfRows(self.minString.count, withRowType:"PredCell")
        for (index,rowModel) in title2.enumerated() {
            if let rowController12 = tbl_map.rowController(at: index) as? MapCell{
                print(title2[index]); rowController12.lbl_prediction.setText(title2[index])
            }
        }
        print("Min Sring",self.minString)
        for (index1,rowModel) in minString.enumerated() {
            if let rowController12 = tbl_map2.rowController(at: index1) as? PredCell{
                rowController12.lbl_pred.setText(minString[index1])
            }
        }
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String])
    {
//        element = elementName as NSString
//        print(element)
//        if elementName == "route" {
//            //let tempTag = Tag();
//            if let name = attributeDict["title"] {
//                self.title1.append(name)
//                //                title2.append(name)
//                //                titleTagDict.setValue(name, forKey: "title")
//                //  self.tbl_nearBy.reloadData()
//            }
//        }
        if (serviceTag == "1"){
            element = elementName as NSString
            print(element)
            if elementName == "stop" || elementName == "vehicle"{
                //let tempTag = Tag();
                if let name = attributeDict["lat"] {
                    self.busLat.append(name)
                    // self.tbl_nearBy.reloadData()
                }
                if let tag = attributeDict["lon"] {
                    self.busLon.append(tag)

                  //  self.lon1.append(tag)
                    //self.tbl_nearBy.reloadData()
                }
                if let title12 = attributeDict["title"] {
                    self.title1.append(title12)
                    //self.tbl_nearBy.reloadData()
                    self.titleBusStop.append(title12)
                }
                if let tag = attributeDict["tag"] {
                    self.tagArray.append(tag)
                    print("TagArray",self.tagArray)
                    //self.tbl_nearBy.reloadData()
                }
                //            if let c = attributeDict["count"] {
                //                if let count = Int(c) {
                //                    tempTag.count = count;
                //                }
                //            }
                //            self.item.tag.append(tempTag);
            }}else if (self.serviceTag == "2"){
            print("Inside service tag 2")
            print(elementName)
            if elementName == "predictions"{
              //  print("AttributeDict",attributeDict)
                
                if let title123 = attributeDict["stopTitle"] {
                    // self.toptitle = title123
//                    self.stopTitle.text = title123
//                    print("TopTitle",toptitle)
                    //self.tbl_nearBy.reloadData()
                }
            }
            if elementName == "direction"{
             //   print("AttributeDict",attributeDict)
                if let title12 = attributeDict["title"] {
//                    self.toptitle = title12
//                    self.directionTitle.text = self.toptitle
//                    print("TopTitle",toptitle)
                    //self.tbl_nearBy.reloadData()
                }
            }
            if elementName == "prediction"{
                print("AttributeDict",attributeDict)
                if let min = attributeDict["minutes"] {
                   
                    
                    self.minString.append(min+"m")
                    //self.toptitle = title12
//                    self.min_label.text = min + "m"
//                    print("TopTitle",toptitle)
                    //self.tbl_nearBy.reloadData()
                }
            }
        }
    }
//    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
//        pushController(withName: "MapVC", context: title2[rowIndex])
//    }
    func parserDidEndDocument(_ parser: XMLParser) {
        DispatchQueue.main.async {
            self.nearByBusStop()
           
        }
    }
}
