//
//  MapViewController.swift
//  laundry
//
//  Created by santosh tekulapally on 2019-11-21.
//  Copyright © 2019 santosh tekulapally. All rights reserved.
//
@available(iOS 10.0, *)
let appDel = UIApplication.shared.delegate as! AppDelegate

import UIKit
import GooglePlaces
import GoogleMaps
import MapKit

class DestinationVC: UIViewController,GMSMapViewDelegate,GMSAutocompleteViewControllerDelegate,CLLocationManagerDelegate {
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
      var locManager = CLLocationManager()
    @IBOutlet weak var pickLoacionView: UIView!
    @IBOutlet var const_bottom: NSLayoutConstraint!
    var userSelectedCurrent = false
    var selectedcoordinate : CLLocationCoordinate2D? = nil
    var Googleaddress :GMSAddress?
    var isFromViewController = ""
    var addressIs = ""

    var latLog = ""
    var distance = Double()
    var destinalocationLat = Double()
        var destinationlocationLong = Double()
    var gotnotification = false
    var locationLat = Double()
     var locationLong = Double()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.gotnotification = false
        
        let timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(getloc), userInfo: nil, repeats: true)

      
       
      
        
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        pickLoacionView.layer.cornerRadius = 2
        // Do any additional setup after loading the view.
        
        
        locManager.requestWhenInUseAuthorization()

               if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
                   CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
                   guard let currentLocation = locManager.location else {
                       return
                   }
                
                 locManager.startUpdatingLocation()
                   print(currentLocation.coordinate.latitude)
                   print(currentLocation.coordinate.longitude)
                   self.locationLat = currentLocation.coordinate.latitude
                   self.locationLong = currentLocation.coordinate.longitude
                   
                   
               }
        
        
        
      //  self.CurentLocation()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(searchLocation))
        tapGesture.numberOfTapsRequired = 1
        pickLoacionView.isUserInteractionEnabled = true
//        if isFromViewController == "ChangeDetails"{
//            pickLoacionView.isUserInteractionEnabled = false
//
//        }
        pickLoacionView.addGestureRecognizer(tapGesture)
        mapView.delegate  = self
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(nextBarButton))
//        self.navigationItem.backBarButtonItem?.title = ""
//        self.navigationItem.title = addressIs
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.title  = "Destination"
        self.CurentLocation()
//        if isFromViewController == "ChangeDetails"{
//            self.const_bottom.constant = 0
//            CurentLocationFromEdit()
//        }else{
//            CurentLocation()
//            self.const_bottom.constant = 0
//        }
        print()
    }
  
    @objc func getloc()
           {
           if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
                      CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
                      guard let currentLocation = locManager.location else {
                          return
                      }
                      locManager.desiredAccuracy  = kCLLocationAccuracyBest;
                      locManager.startUpdatingLocation()

//                      print(currentLocation.coordinate.latitude)
//                      print(currentLocation.coordinate.longitude)
                      self.locationLat = currentLocation.coordinate.latitude
                      self.locationLong = currentLocation.coordinate.longitude
                      
                      
                  }
           
            self.checkdistance()

           }
    
    @objc func sendnotification() {
        
        
        
        print ("sent from the original notification")

           UNUserNotificationCenter.current().getNotificationSettings { (settings) in
               
               // we're only going to create and schedule a notification
               // if the user has kept notifications authorized for this app
               guard settings.authorizationStatus == .authorized else { return }
               
               // create the content and style for the local notification
               let content = UNMutableNotificationContent()
               
               // #2.1 - "Assign a value to this property that matches the identifier
               // property of one of the UNNotificationCategory objects you
               // previously registered with your app."
               content.categoryIdentifier = "Transit Notify"
               
               // create the notification's content to be presented
               // to the user
               content.title = "You are about to reach your destination"
               content.subtitle = "Please be ready.."
            content.body = "Your destination is 500 meters away"
               content.sound = UNNotificationSound.default
               
               // #2.2 - create a "trigger condition that causes a notification
               // to be delivered after the specified amount of time elapses";
               // deliver after 10 seconds
               let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
               
               // create a "request to schedule a local notification, which
               // includes the content of the notification and the trigger conditions for delivery"
               let uuidString = UUID().uuidString
               let request = UNNotificationRequest(identifier: uuidString, content: content, trigger: trigger)
               
               // "Upon calling this method, the system begins tracking the
               // trigger conditions associated with your request. When the
               // trigger condition is met, the system delivers your notification."
               UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
            self.gotnotification = true

               
           } // end getNotificationSettings
           
        
       }
//    @available(iOS 10.0, *)
//    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//       completionHandler([.alert, .badge, .sound])
//    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        
        let location = locations.last! as CLLocation
        
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        //print("Locations",location)
        self.locationLat = location.coordinate.latitude
        self.locationLong = location.coordinate.longitude
       //print("person lat aree",self.locationLat)
    // print("person long aree",self.locationLong)
        
       
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.title  = ""
    }
    
//    @IBAction func done(_ sender: Any) {
//        self.dismiss(animated:true, completion:nil)
//
//    }
    
//    @IBAction func CancelC(_ sender: Any) {
//       // _ = self.navigationController?.popViewController(animated: true)
//        self.dismiss(animated:true, completion:nil)
//
//    }
    @IBOutlet var cancelC: UIButton!
    //MARK:-To push the viewController To the SaveLocation
    @objc func nextBarButton(){
       // _ = self.navigationController?.popViewController(animated: true)
    }
    //MARK:-To get the current location
    func CurentLocation(){
        if #available(iOS 10.0, *) {
            let camera = GMSCameraPosition.camera(withLatitude: 43.82230635 , longitude: -79.40331973, zoom: 16.0)
            mapView.camera = camera
            //  mapView.isMyLocationEnabled = true
            mapView.settings.allowScrollGesturesDuringRotateOrZoom = false

        } else {
            // Fallback on earlier versions
//            let appDelegate = UIApplication.shared.delegate
//            //let aVariable = appDelegate.someVariable
//            //let camera = GMSCameraPosition.camera(withLatitude:(latitude)!, longitude: (longitude)!, zoom: 16.0)
//
//            let camera = GMSCameraPosition.camera(withLatitude: appDelegate.currentLocation1.coordinate.latitude , longitude: appDelegate.currentLocation1.coordinate.longitude, zoom: 16.0)
//            mapView.camera = camera
//            //  mapView.isMyLocationEnabled = true
//            mapView.settings.allowScrollGesturesDuringRotateOrZoom = false

        }
    }
    func CurentLocationFromEdit(){
        if #available(iOS 10.0, *) {
            let seaperator = latLog.components(separatedBy:",")
            let lat = seaperator[0].replacingOccurrences(of: " ", with: "")
            let long = seaperator[1].replacingOccurrences(of: " ", with: "")
            //escapedString!
            
            let latitude = Double(lat)
            let longitude = Double(long)

            let camera = GMSCameraPosition.camera(withLatitude: latitude! , longitude: longitude!, zoom: 16.0)
            mapView.camera = camera
            //  mapView.isMyLocationEnabled = true
            mapView.settings.allowScrollGesturesDuringRotateOrZoom = false
            
        } else {
            // Fallback on earlier versions
            let seaperator = latLog.components(separatedBy:",")
            let lat = seaperator[0].replacingOccurrences(of: " ", with: "")
            let long = seaperator[1].replacingOccurrences(of: " ", with: "")
            //escapedString!
            
            let latitude = Double(lat)
            let longitude = Double(long)
            
            let camera = GMSCameraPosition.camera(withLatitude: latitude! , longitude: longitude!, zoom: 16.0)
            mapView.camera = camera
            //  mapView.isMyLocationEnabled = true
            mapView.settings.allowScrollGesturesDuringRotateOrZoom = false

        }
    }
    //MARK:-To get the coordinate of the location
    func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D) {
        // 1
        let geocoder = GMSGeocoder()
        let geocoder1 = CLGeocoder()
         let getMovedMapCenter: CLLocation =  CLLocation(latitude:coordinate.latitude, longitude: coordinate.longitude)
        
        
//        geocoder1.reverseGeocodeLocation(getMovedMapCenter) { (placemarksArray, error) in
//
//            if (placemarksArray?.count)! > 0 {
//                print("array",placemarksArray)
//                let placemark = placemarksArray?.first
//                print("PlaceMark",placemark?.addressDictionary)
//                //                    let number = placemark!.subThoroughfare
//                let bairro = placemark!.name
//                print("placeName",bairro)
//                // UserPreference.saveLocation(str: bairro ?? "")
//                //                    let street = placemark!.thoroughfare
//                //                    let pinCode = placemark?.postalCode
//
//                //                    print("PlaceMark:",placemark)
//                //print("Bairo:",bairro ?? <#default value#>)
//                //                    print("Street",street)
//                //          UserPreference.saveLocation(long: bairro ?? "")
//                //                    print("Number",number)
//                //                    print("Pincode:",pinCode)
//                // print("%@%@%@",number ?? 00,bairro ?? "",street ?? """)
//                //  self.addressLabel.text = "\(street!), \(number!) - \(bairro!)"
//            }
//        }
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            print("total address",response?.results())
            if let address = response?.firstResult() {
                // 3
              //  print("gmsaddress",address)
                let lines = address.lines! as [String]
                self.addressLabel.text = lines.joined(separator: "\n")
                self.addressLabel.adjustsFontSizeToFitWidth = true
                self.selectedcoordinate = coordinate
                // 4
                self.Googleaddress = address
                self.destinalocationLat =  Double(coordinate.latitude)
                self.destinationlocationLong = Double(coordinate.longitude)
//                UserPreference.saveLatitude(str:latstr)
//                UserPreference.saveLongitude(str:longStr)
              //  self.GetAddress(lat: latstr, long: longStr)
//                UserPreference.saveLocation(str: address.locality!)
//                UserPreference.saveAddress(str: self.addressLabel.text!)

              //  print("coordinate aree",coordinate)
            
              //  print("destinations lat is ",self.destinalocationLat)
              //  print("destistination long is ",self.destinationlocationLong)
               

                let newaddressIs = address.lines! as [String]
                    print(newaddressIs)
                
                
                
                let MyAddress = address.subLocality
               //  print("myaddress:",MyAddress ?? "")
               

                self.userSelectedCurrent = false
                UIView.animate(withDuration: 0.25) {
                    self.view.layoutIfNeeded()
                    self.checkdistance()

                }
            }
        }
        
        
    }
    
    func checkdistance()  {
        
        let myLocation = CLLocation(latitude: self.locationLat, longitude:self.locationLong)
        
        
        
        
        print("the user present  lat is ",  self.locationLat)
        
        print("the user present long is ",  self.locationLong)
        
        print("destinations lat is ",self.destinalocationLat)
        
        print("destistination long is ",self.destinationlocationLong)
        
        //My buddy's location
        let myBuddysLocation = CLLocation(latitude: self.destinalocationLat , longitude: self.destinationlocationLong)

        //Measuring my distance to my buddy's (in km)
        self.distance = myLocation.distance(from: myBuddysLocation) / 1000

        //Display the result in km
        print(String(format: "The distance to my buddy is %.01fkm", self.distance))
        
        
        if (self.distance < 0.5) {
            
            if(self.gotnotification == false)
            {
                print ("sent notification")
                sendnotification()

            }
            else if(self.gotnotification == true){
                
               // sendnotification()

                print ("no notification sent")


            }
                
        }else{
            
        }
        
    }
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        UIView.animate(withDuration: 0.25) {
            self.navigationController?.isNavigationBarHidden = false
            self.view.layoutIfNeeded()
        }
        reverseGeocodeCoordinate(coordinate: position.target)
    }
    @objc func searchLocation(){
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        self.present(acController, animated: true, completion: nil)
    }
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
    }
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if  self.userSelectedCurrent == false{
            UIView.animate(withDuration: 0.5) {
                self.gotnotification = false
                self.navigationController?.isNavigationBarHidden = true
                self.view.layoutIfNeeded()
            }
        }
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.dismiss(animated: true, completion: nil)
        let coordinates = place.coordinate
        let camera = GMSCameraPosition.camera(withLatitude: coordinates.latitude , longitude: coordinates.longitude, zoom: 16.0)
        mapView.camera = camera
        mapView.isMyLocationEnabled = true
        mapView.settings.allowScrollGesturesDuringRotateOrZoom = false
        
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("Autocomplete was cancelled.")
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
