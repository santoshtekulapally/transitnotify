//
//  MapViewController.swift
//  Transit Notify
//
//  Created by santosh tekulapally on 2019-11-21.
//  Copyright © 2019 santosh tekulapally. All rights reserved.
//

import UIKit
import GoogleMaps
class MapViewController: UIViewController,XMLParserDelegate,CLLocationManagerDelegate,UICollectionViewDataSource,UICollectionViewDelegate{
    var locations = [CLLocation]()
    var closestLocation: CLLocation?
    var smallestDistance: CLLocationDistance?
    var currentLocation:CLLocation?
    var arrIndex = [Int]()
    var toptitle = ""
    var tagString = ""
    var arrayAdd = [""]
    var locationLat = Double()
    var locationLong = Double()
    var parser = XMLParser()
    var posts = NSMutableArray()
    var elements = NSMutableDictionary()
    @IBOutlet weak var min_label: UILabel!
    
    @IBOutlet weak var Fourth_min_label: UILabel!
    @IBOutlet weak var third_min_label: UILabel!
    @IBOutlet weak var first_min_label: UILabel!
    var element = NSString()
    var lat1 = [String]()
    var lon1 = [String]()
    var lat2 = [String]()
    var lon2 = [String]()
    var latBus = [String]()
    var lonBus = [String]()
     var minutesarray = [String]()
    @IBOutlet weak var stopTitle: UILabel!
    @IBOutlet weak var directionTitle: UILabel!
    @IBOutlet weak var titleStr: UILabel!
    var titleArr = ["Hello","Hi"]
    var tagArray = [String]()
    var date = NSMutableString()
    var locationManager = CLLocationManager()
    var direction = ["East","West","South","North"]
    let baseURLDirections = "https://maps.googleapis.com/maps/api/directions/json?"
    var serviceTag = ""
    var selectedRoute: Dictionary<NSObject, AnyObject>!
    
    var overviewPolyline: Dictionary<NSObject, AnyObject>!
    
    var originCoordinate: CLLocationCoordinate2D!
    
    var destinationCoordinate: CLLocationCoordinate2D!
    
    var originAddress: String!
    
    @IBOutlet weak var coll_sides: UICollectionView!
    var destinationAddress: String!
    var didFindMyLocation = false
    @IBOutlet weak var GmsMapViewRef: GMSMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //  DispatchQueue.main.async {
             let add = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(refreshtapped))
                add.tintColor = UIColor.white
        //        let play = UIBarButtonItem(title: "Play", style: .plain, target: self, action: #selector(playTapped))
                
                navigationItem.rightBarButtonItems = [add]

        
        //  }
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
 
        
    }
    //    override func viewWillDisappear(_ animated: Bool) {
    //        super.viewWillDisappear(true)
    //        self.view.removeFromSuperview()
    //    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        DispatchQueue.main.async {
            self.beginParsing()
            self.hitParsing()
            self.locationParsing()
            // self.addMap()
        }
        self.titleStr.text = self.tagString
    }
    
     @objc func refreshtapped(){
        
       super.viewWillAppear(true)
             
             DispatchQueue.main.async {
                 self.beginParsing()
                 self.hitParsing()
                 self.locationParsing()
                 // self.addMap()
             }
             self.titleStr.text = self.tagString
        
        }
    func drawPath()
    {
        let origin = "\(self.lat1[0]),\(self.lon1[0])"
        let destination = "\(self.lat1[self.lat1.count-1]),\(self.lon1[self.lon1.count-1])"
        
        print(origin)
        print(destination)
        print("Lat1",self.lat1)
        print("Lan",self.lon1)
        //        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=AIzaSyBDNQN1SPw803D32ihmOwcEO1IAJzdrupU"
        
        let url = URL(string:"https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=AIzaSyBDNQN1SPw803D32ihmOwcEO1IAJzdrupU")
        // print(url)
        let session = URLSession.shared
        let task = session.dataTask(with: url!) { (data, response, error) in
            if error != nil && data != nil{
                print("error")
            }
            guard let response = response as? HTTPURLResponse,(200...299).contains(response.statusCode)else {
                print("Server error")
                return
            }
            
            //            guard let mimetype = response.mimeType,mimetype == "application/json" else{
            //                print("Wrong MIME type!")
            //                return
            //
            //            }
            do{
                let json = try JSONSerialization.jsonObject(with: data!, options: [])
                // print(json)
                let json1 = json as! NSDictionary
                let routes = json1.value(forKey: "routes") as! [NSDictionary]
                
                for route in routes
                {
                    let routeOverviewPolyline = route.value(forKey: "overview_polyline") as! NSDictionary
                    let points = routeOverviewPolyline.value(forKey: "points") as? String ?? ""
                    DispatchQueue.main.async {
                        //     self.tbl_task.reloadData()
                        let cameraPositionCoordinates = CLLocationCoordinate2D(latitude:Double(self.lat1[0]) as! CLLocationDegrees , longitude: Double(self.lon1[0]) as! CLLocationDegrees )
                        let cameraPosition = GMSCameraPosition.camera(withTarget: cameraPositionCoordinates, zoom: 12)
                        let mapView = GMSMapView.map(withFrame: CGRect(x: 100, y: 100, width: 200, height: 200), camera: cameraPosition)
                        //  mapView.isMyLocationEnabled = true
                        var bounds = GMSCoordinateBounds()
                        for i in 0...self.lat1.count-1
                        {
                            let latitude = self.lat1[i]
                            let longitude = self.lon1[i]
                            
                            let marker = GMSMarker()
                            marker.position = CLLocationCoordinate2D(latitude:Double(latitude) as! CLLocationDegrees, longitude:Double(latitude) as! CLLocationDegrees)
                            marker.map = mapView
                            bounds = bounds.includingCoordinate(marker.position)
                        }
                        let update = GMSCameraUpdate.fit(bounds, withPadding: 100)
                        mapView.animate(with: update)
                        
                        //                        let path = GMSPath.init(fromEncodedPath: points)
                        //                        let polyline = GMSPolyline.init(path: path)
                        //                      //  polyline.map = self.GmsMapViewRef
                        //                       // self.GmsMapViewRef = polyline.map
                        //                        polyline.map = mapView
                        //                        polyline.strokeWidth = 3.0
                        
                        //  polyline.strokeColor = .red
                        self.view = mapView
                        
                    }
                    
                }
                //                let dict = json as! NSDictionary
                //                let routes = dict["routes"].arrayValue
                //
                //                for route in routes
                //                {
                //                    let routeOverviewPolyline = route["overview_polyline"].dictionary
                //                    let points = routeOverviewPolyline?["points"]?.stringValue
                //                    let path = GMSPath.init(fromEncodedPath: points!)
                //                    let polyline = GMSPolyline.init(path: path)
                //                    polyline.map = self.mapView
                //                }
                
                //                self.arrayData2 =  (json as! NSDictionary).value(forKey: "hits") as! NSArray
                //                print("second",self.arrayData)
                //                self.arrayData.adding(self.arrayData2)
                //                print("second12",self.arrayData)
                //                print("ArrayData count2",self.arrayData.count)
                
                print(json)
            }catch{
                print("JSON error: \(error.localizedDescription)")
                
            }
            
        }
        task.resume()
        //        Alamofire.request(url).responseJSON { response in
        //            print(response.request)  // original URL request
        //            print(response.response) // HTTP URL response
        //            print(response.data)     // server data
        //            print(response.result)   // result of response serialization
        //
        //            let json = JSON(data: response.data!)
        //            let routes = json["routes"].arrayValue
        //
        //            for route in routes
        //            {
        //                let routeOverviewPolyline = route["overview_polyline"].dictionary
        //                let points = routeOverviewPolyline?["points"]?.stringValue
        //                let path = GMSPath.init(fromEncodedPath: points!)
        //                let polyline = GMSPolyline.init(path: path)
        //                polyline.map = self.mapView
        //            }
        //        }
    }
    func parserDidEndDocument(_ parser: XMLParser) {
        DispatchQueue.main.async {
            // Update UI
            //self.displayOnUI()
            self.addMap()
            self.addBusLocation()
           // self.filter()
        }
    }
    
    func addBusLocation(){
        print(self.latBus)
        if self.latBus.count != 0{
         DispatchQueue.main.async {
            for i in 0...self.latBus.count-1{
                let marker2 = GMSMarker()
                let markerImage = UIImage(named: "red")!.withRenderingMode(.alwaysTemplate)
                
                
                
                marker2.icon = UIImage(named: "red")!.withRenderingMode(.alwaysTemplate)
                
                // marker.iconView = markerView
                marker2.position = CLLocationCoordinate2D(latitude:Double(self.latBus[i]) as! CLLocationDegrees, longitude:Double(self.lonBus[i]) as! CLLocationDegrees)
               // marker2.title = self.titleArr[i]
                
                marker2.map = self.GmsMapViewRef

            }
            }}else{
            
            let alert = UIAlertController(title: "Alert", message: "Bus on hold.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    func addMap(){
        DispatchQueue.main.async {
            //     self.tbl_task.reloadData()
            let cameraPositionCoordinates = CLLocationCoordinate2D(latitude: Double( self.lat1[0]) as! CLLocationDegrees, longitude: Double(self.lon1[0]) as! CLLocationDegrees)
            print("Coordinaes:",cameraPositionCoordinates)
            let cameraPosition = GMSCameraPosition.camera(withTarget: cameraPositionCoordinates, zoom: 12)
            //           self.GmsMapViewRef = GMSMapView.map(withFrame: CGRect(x: 100, y: 100, width: 200, height: 200), camera: cameraPosition)
            self.GmsMapViewRef.camera = cameraPosition
            //  mapView.isMyLocationEnabled = true
            var bounds = GMSCoordinateBounds()
            let path = GMSMutablePath()
            for i in 0...self.lat1.count-1
            {
                let latitude = self.lat1[i]
                let longitude = self.lon1[i]
                path.add(CLLocationCoordinate2D(latitude: Double(latitude) as! CLLocationDegrees, longitude: Double(longitude) as! CLLocationDegrees))
                
                let marker = GMSMarker()
                let markerImage = UIImage(named: "redCircle1")!.withRenderingMode(.alwaysTemplate)
                
                //creating a marker view
                //                let markerView = UIImageView(image: markerImage)
                
                //changing the tint color of the image
                //markerView.tintColor = UIColor.red
                
                marker.icon = UIImage(named: "redCircle1")!.withRenderingMode(.alwaysTemplate)
                
                // marker.iconView = markerView
                marker.position = CLLocationCoordinate2D(latitude:Double(latitude) as! CLLocationDegrees, longitude:Double(longitude) as! CLLocationDegrees)
                marker.title = self.titleArr[i]
                
                marker.map = self.GmsMapViewRef
                bounds = bounds.includingCoordinate(marker.position)
            }
            let polyline = GMSPolyline(path: path)
            polyline.map = self.GmsMapViewRef
            polyline.strokeColor = .red
            polyline.strokeWidth = 4.0
            let update = GMSCameraUpdate.fit(bounds, withPadding: 100)
            self.GmsMapViewRef.animate(with: update)
            // self.view = self.GmsMapViewRef
            
            //                        let path = GMSPath.init(fromEncodedPath: points)
            //                        let polyline = GMSPolyline.init(path: path)
            //                      //  polyline.map = self.GmsMapViewRef
            //                       // self.GmsMapViewRef = polyline.map
            //                        polyline.map = mapView
            //                        polyline.strokeWidth = 3.0
            
            //  polyline.strokeColor = .red
            
            
        }
        
    }
    func filter(){
        let currentLocationCal = CLLocation(latitude:Double(lat1[0]) as! CLLocationDegrees, longitude:Double(lon1[0]) as! CLLocationDegrees)
        
        for (index,location) in lat1.enumerated() {
            let locationCl = CLLocation(latitude:Double(lat1[index]) as! CLLocationDegrees, longitude:Double(lon1[index]) as! CLLocationDegrees )
            self.locations.append(locationCl)
            print("Location Array",self.locations)
            let distance = currentLocationCal.distance(from: locationCl)
            print("Distance:",distance)
            if Int(distance) == 0 || Int(distance) < 300 {
//                closestLocation = locationCl
//                arrIndex.append(index)
//                self.titleFilter.append(title1[index])
//                self.filterTag.append(tagArray[index])
                //smallestDistance = distance
                print("Index Location Array",self.arrIndex)
              //  self.tbl_nearBy.reloadData()
                self.lat2.append(lat1[index])
                self.lon2.append(lon1[index])
                print("Lat1 print",lat2)
            }
            
        }
        self.addMap()
        //        let currentLocationCal = CLLocation(latitude:43.66676999, longitude:-79.3373033)
        print("Current location",currentLocationCal)
        //        for (index,location) in locations.enumerated() {
        //            let distance = currentLocationCal.distance(from: location)
        //            print("Distance:",distance)
        //            if Int(distance) == 0 || Int(distance) < 1000 {
        //                closestLocation = location
        //                arrIndex.append(index)
        //                //smallestDistance = distance
        //                print("Index Location Array",self.arrIndex)
        //
        //            }
        //        }
    }
    private func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedWhenInUse {
            //  GmsMapViewRef.isMyLocationEnabled = true
        }
    }
    //    func getDirections(origin: String!, destination: String!, waypoints: Array<String>!, travelMode: AnyObject!, completionHandler: ((_ status: String, _ success: Bool) -> Void)) {
    //        if let originLocation = origin {
    //            if let destinationLocation = destination {
    //                var directionsURLString = baseURLDirections + "origin=" + originLocation + "&destination=" + destinationLocation
    //              let urlwithPercentEscapes = directionsURLString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
    //                //else { return "" }
    //
    ////                directionsURLString = directionsURLString.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
    //                let directionsURL = NSURL(string: urlwithPercentEscapes!)
    //
    //
    //                dispatch_async(dispatch_get_main_queue(), { () -> Void in
    //                    let directionsData = NSData(contentsOfURL: directionsURL!)
    //
    //                    var error: NSError?
    //                    let dictionary: Dictionary<NSObject, AnyObject> = NSJSONSerialization.JSONObjectWithData(directionsData!, options: NSJSONReadingOptions.MutableContainers, error: &error) as Dictionary<NSObject, AnyObject>
    //
    //                    if (error != nil) {
    //                        println(error)
    //                        completionHandler(status: "", success: false)
    //                    }
    //                    else {
    //
    //                    }
    //                })
    //            }
    //            else {
    //                completionHandler("Destination is nil.", false)
    //            }
    //        }
    //        else {
    //            completionHandler("Origin is nil", false)
    //        }
    //    }
    //    func observeValueForKeyPath(keyPath: String, ofObject object: AnyObject, change: [NSObject : AnyObject], context: UnsafeMutableRawPointer) {
    //        if !didFindMyLocation {
    //            let myLocation: CLLocation = change[NSKeyValueChangeKey] as CLLocation
    //            GmsMapViewRef.camera = GMSCameraPosition.camera(withTarget: myLocation.coordinate, zoom: 10.0)
    //            GmsMapViewRef.settings.myLocationButton = true
    //
    //            didFindMyLocation = true
    //        }
    //    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"CollectionDirectionCell", for: indexPath) as! CollectionDirectionCell
        cell.lbl_name.text = direction[indexPath.row]
        return cell
        
    }
    func beginParsing()
        
    {
        self.serviceTag = "1"
        let concatString = ""
        let fullNameArr = self.tagString.components(separatedBy: "-")

        posts = []
        parser = XMLParser(contentsOf:(NSURL(string:"http://webservices.nextbus.com/service/publicXMLFeed?command=routeConfig&a=ttc&r=\(fullNameArr[0])"))! as URL)!
        //        print("http://webservices.nextbus.com/service/publicXMLFeed?command=routeConfig&a=ttc&r=\(self.tagString)")
        parser.delegate = self
        parser.parse()
        // tbl_nearBy!.reloadData()
    }
    
    func hitParsing()
    {
         let fullNameArr1 = self.tagString.components(separatedBy: "-")
        self.serviceTag = "2"
        posts = []
        parser = XMLParser(contentsOf:(NSURL(string:"http://webservices.nextbus.com/service/publicXMLFeed?command=predictions&&a=ttc&r=\(fullNameArr1[0])&s=\(self.tagArray[5])&useShortTitles=true"))! as URL)!
      //  print("tags are",tagArray)

        //        //print("http://webservices.nextbus.com/service/publicXMLFeed?command=routeConfig&a=ttc&r=\(self.tagString)")
        parser.delegate = self
        parser.parse()
        // tbl_nearBy!.reloadData()
    }
    func locationParsing()
    {
        let concatString = ""
        let fullNameArr1 = self.tagString.components(separatedBy: "-")
        print("Full Array Loation",fullNameArr1)
        self.serviceTag = "3"
        posts = []
        parser = XMLParser(contentsOf:(NSURL(string:"http://webservices.nextbus.com/service/publicXMLFeed?command=vehicleLocations&a=ttc&r=\(fullNameArr1[0])&t=0"))! as URL)!
        //print("http://webservices.nextbus//.com/service/publicXMLFeed?command=vehicleLocations&a=ttc&r=\(fullNameArr1[0])&t=0")
        //        //print("http://webservices.nextbus.com/service/publicXMLFeed?command=routeConfig&a=ttc&r=\(self.tagString)")
        
        
        parser.delegate = self
        parser.parse()
        // tbl_nearBy!.reloadData()
    }
    //    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
    //        // self.drawPath()
    //    }
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        //        if elementName == "stop" {
        //
        //
        //        }
    }
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String])
    {
        if (serviceTag == "1"){
        element = elementName as NSString
        print(element)
        if elementName == "stop" || elementName == "vehicle"{
            //let tempTag = Tag();
            if let name = attributeDict["lat"] {
                self.lat1.append(name)
                // self.tbl_nearBy.reloadData()
            }
            if let tag = attributeDict["lon"] {
                self.lon1.append(tag)
                //self.tbl_nearBy.reloadData()
            }
            if let title12 = attributeDict["title"] {
                self.titleArr.append(title12)
                //self.tbl_nearBy.reloadData()
            }
            if let tag = attributeDict["tag"] {
                self.tagArray.append(tag)
                //self.tbl_nearBy.reloadData()
            }
            
            }}else  if (serviceTag == "2"){
            print("Inside service tag 2")
            print(elementName)
            if elementName == "predictions"{
                print("AttributeDict",attributeDict)
                
                if let title123 = attributeDict["stopTitle"] {
                   // self.toptitle = title123
                    self.stopTitle.text = title123
                    print("TopTitle",toptitle)
                    //self.tbl_nearBy.reloadData()
                }
            }
            if elementName == "direction"{
                print("AttributeDict",attributeDict)
                if let title12 = attributeDict["title"] {
                    self.toptitle = title12
                    self.directionTitle.text = self.toptitle
                    print("TopTitle",toptitle)
                    //self.tbl_nearBy.reloadData()
                }
            }
            if elementName == "prediction"{
                print("AttributeDict",attributeDict)
                
                
                
                if let minutes = attributeDict["minutes"]
                {
                   self.minutesarray.append(minutes)

                    print("TopTitle",minutesarray)

                    
                    if(minutesarray.count == 1){
                        self.min_label.text = minutesarray.first! + "mins"

                        
                       // self.third_min_label.text = minutesarray[2]
                    }
                    else if(minutesarray.count == 2){
                        self.third_min_label.text = minutesarray[1] + "mins"
                      //  self.first_min_label.text = minutesarray[1]
                       // self.third_min_label.text = minutesarray[2]
                    }
                    else if(minutesarray.count == 3){
                        self.Fourth_min_label.text = minutesarray[2] + "mins"
                      //  self.first_min_label.text = minutesarray[1]
                       // self.third_min_label.text = minutesarray[2]
                    }
                    

                    
                    
                    //self.tbl_nearBy.reloadData()
                }
            }
        }else{
            print("third element name",elementName)
            if elementName == "vehicle"{
                print("AttributeDict",attributeDict)
                if let min = attributeDict["lat"] {
                    self.latBus.append(min)
                    //self.toptitle = title12
                   // self.min_label.text = min + "m"
                    print("time in seconds",toptitle)
                    //self.tbl_nearBy.reloadData()
                }
                if let min = attributeDict["lon"] {
                    self.lonBus.append(min)
                    //self.toptitle = title12
                    // self.min_label.text = min + "m"
                    //   print("TopTitle",toptitle)
                    //self.tbl_nearBy.reloadData()
                }
            }
        }
            
        //        if (elementName as NSString).isEqual(to: "route")
        //        {
        //            elements = NSMutableDictionary()
        //            elements = [:]
        //            title1 = NSMutableString()
        //            title1 = ""
        //            date = NSMutableString()
        //            date = ""
        //        }
    }
    
    
    //    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    //    {
    //
    //        let location = locations.last! as CLLocation
    //
    //        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
    //        print("Locations",location)
    //        self.locationLat = 43.66676999   //location.coordinate.latitude
    //        self.locationLong = -79.3373033//location.coordinate.longitude
    //        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
    //        // self.apiCall()
    //        //  self.map.setRegion(region, animated: true)
    //    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
