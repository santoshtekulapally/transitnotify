//
//  FavVC.swift
//  Transit Notify
//
//  Created by santosh tekulapally on 2019-11-21.
//  Copyright © 2019 santosh tekulapally. All rights reserved.

import UIKit

class FavVC: UIViewController,UITableViewDelegate,UITableViewDataSource{
    var title1 = [String]()

    @IBOutlet weak var tbl_fav: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.title1 = UserDefaults.standard.value(forKey: "SavedFav") as? [String] ?? [String]()
        print(self.title1)
        self.tbl_fav.reloadData()

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.loadData),
            name: NSNotification.Name(rawValue: "reloadData"),
            object: nil)
        // Do any additional setup after loading the view.
    }
     @objc func addTapped(){
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DestinationVC") as? DestinationVC
    //    vc?.tagString = self.title1[indexPath.row]
        self.navigationController?.present(vc!, animated: true)
        }
    @objc func loadData(notification: NSNotification){
        self.title1 = UserDefaults.standard.value(forKey: "SavedFav") as? [String] ?? [String]()
        print(self.title1)
        self.tbl_fav.reloadData()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return title1.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransitTblCell", for: indexPath) as! TransitTblCell
        cell.lbl_title.text = title1[indexPath.row]
        cell.favorite.tag = indexPath.row
//        cell.favorite.addTarget(self, action: #selector(favButtonClicked), for: .touchUpInside)
//        if arrayFav.contains(indexPath.row){
//            let image = UIImage(named: "Favorite")
//            cell.favorite.setImage(image, for:.normal)
//        }else{
//            let image = UIImage(named: "unFave")
//            cell.favorite.setImage(image, for:.normal)
//            
//        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MapViewController") as? MapViewController
        vc?.tagString = self.title1[indexPath.row]
        self.navigationController?.pushViewController(vc!, animated: true)
    }
  
}
