//
//  TransitTblCell.swift
//  Transit Notify
//
//  Created by santosh tekulapally on 2019-11-21.
//  Copyright © 2019 santosh tekulapally. All rights reserved.
//

import UIKit

class TransitTblCell: UITableViewCell {

    @IBOutlet weak var lbl_toronto: UILabel!
    @IBOutlet weak var lbl_mainLocation: UILabel!
    @IBOutlet weak var lbl_titleLocation: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    
    
    
    @IBOutlet weak var favorite: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
