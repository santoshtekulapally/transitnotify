//
//  NearByVC.swift
//  Transit Notify
//
//  Created by santosh tekulapally on 2019-11-21.
//  Copyright © 2019 santosh tekulapally. All rights reserved.
//

import UIKit
import CoreLocation

import MapKit
class NearByVC: UIViewController,UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate,XMLParserDelegate,UISearchBarDelegate {
   var locManager = CLLocationManager()
     var currentLocation: CLLocation!
    
     var locationManager: CLLocationManager!
    @IBOutlet weak var tbl_nearBy: UITableView!
    var arrayAdd = [""]
    var locationLat = Double()
    var locationLong = Double()
    var parser = XMLParser()
    var posts = NSMutableArray()
    var elements = NSMutableDictionary()
    var element = NSString()
    var title1 = [String]()
     var title2 = [String]()
    var tagArray = [String]()
      var arrSaved = [String]()
    @IBOutlet weak var searchBar: UISearchBar!
    var date = NSMutableString()
    var SearchBarValue:String!
    var searchActive : Bool = false
    var data : NSMutableArray!
    var filtered:NSMutableArray!
    var titleTagDict = NSMutableDictionary()
    var arrayFav = [Int]()
    
    var distance = Double()
       var destinalocationLat = Double()
           var destinationlocationLong = Double()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.arrayFav = UserDefaults.standard.value(forKey: "SavedFav1") as? [Int] ?? [Int]()
        self.arrSaved = UserDefaults.standard.value(forKey: "SavedFav") as? [String] ?? [String]()

       
        
    locManager.requestWhenInUseAuthorization()

        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            guard let currentLocation = locManager.location else {
                return
            }
            locManager.desiredAccuracy  = kCLLocationAccuracyBest;
            locManager.startUpdatingLocation()

            print(currentLocation.coordinate.latitude)
            print(currentLocation.coordinate.longitude)
            self.locationLat = currentLocation.coordinate.latitude
            self.locationLong = currentLocation.coordinate.longitude
            
            
        }
        
   


        self.beginParsing()
      //  UserDefaults.standard.set("Anand", forKey: "name")

//        let add = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
//        add.tintColor = UIColor.white
//        let play = UIBarButtonItem(title: "Play", style: .plain, target: self, action: #selector(playTapped))
        
     //   navigationItem.rightBarButtonItems = [add]

//        locationManager = CLLocationManager()
//        locationManager.delegate = self
//        locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        locationManager.requestAlwaysAuthorization()
//        locationManager.startUpdatingLocation()
//
        // Do any additional setup after loading the view.
        
       
        
       
       
    }
    
    
    
    @objc func getuserlocalways()
           {
           if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
                      CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
                      guard let currentLocation = locManager.location else {
                          return
                      }
                      locManager.desiredAccuracy  = kCLLocationAccuracyBest;
                      locManager.startUpdatingLocation()

                      print(currentLocation.coordinate.latitude)
                      print(currentLocation.coordinate.longitude)
                      self.locationLat = currentLocation.coordinate.latitude
                      self.locationLong = currentLocation.coordinate.longitude
                      
                      
                  }
            print("the updating location is ",  self.locationLat)
            print("the updating location is ",  self.locationLong)
           

           }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    private func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
           if status == CLAuthorizationStatus.authorizedWhenInUse {
               //  GmsMapViewRef.isMyLocationEnabled = true
           }
       }
//   @objc func addTapped(){
//    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DestinationVC") as? DestinationVC
////    vc?.tagString = self.title1[indexPath.row]
//    self.navigationController?.present(vc!, animated: true)
//    }
    func beginParsing()
    {
        posts = []
        parser = XMLParser(contentsOf:(NSURL(string:"http://webservices.nextbus.com/service/publicXMLFeed?command=routeList&a=ttc"))! as URL)!
        parser.delegate = self
        parser.parse()
        tbl_nearBy!.reloadData()
    }
    
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String])
    {
        element = elementName as NSString
        print(element)
        if elementName == "route" {
            //let tempTag = Tag();
            if let name = attributeDict["title"] {
                title1.append(name)
                title2.append(name)
                titleTagDict.setValue(name, forKey: "title")
                self.tbl_nearBy.reloadData()
            }
            if let tag = attributeDict["tag"] {
                self.tagArray.append(tag)
                titleTagDict.setValue(tag, forKey: "tag")
                //self.tbl_nearBy.reloadData()
            }
            print(titleTagDict)
            //            if let c = attributeDict["count"] {
            //                if let count = Int(c) {
            //                    tempTag.count = count;
            //                }
            //            }
            //            self.item.tag.append(tempTag);
        }
//        if (elementName as NSString).isEqual(to: "route")
//        {
//            elements = NSMutableDictionary()
//            elements = [:]
//            title1 = NSMutableString()
//            title1 = ""
//            date = NSMutableString()
//            date = ""
//        }
    }
    
//    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?)
//    {
//        print(elementName)
////        if (elementName as NSString).isEqual(to: "item") {
////            if !title1.isEqual(nil) {
////                elements.setObject(title1, forKey: "title" as NSCopying)
////            }
////            if !date.isEqual(nil) {
////                elements.setObject(date, forKey: "date" as NSCopying)
////            }
////
////            posts.add(elements)
////        }
//    }
//    func parser(parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
//        if elementName == "stop" {
//            //let tempTag = Tag();
//            if let name = attributeDict["title"] {
//                 title1.append(name)
//                self.tbl_nearBy.reloadData()
//            }
////            if let c = attributeDict["count"] {
////                if let count = Int(c) {
////                    tempTag.count = count;
////                }
////            }
////            self.item.tag.append(tempTag);
//        }
//    }
//    func parser(_ parser: XMLParser, foundCharacters string: String)
//    {
//        if element.isEqual(to: "stop") {
//            title1.append(string)
//            print(title1)
//        } else if element.isEqual(to: "pubDate") {
//            date.append(string)
//        }
//    }
   
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        
        let location = locations.last! as CLLocation
        
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        print("Locations",location)
        self.locationLat = location.coordinate.latitude
        self.locationLong = location.coordinate.longitude
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
       // self.apiCall()
        //  self.map.setRegion(region, animated: true)
    }
    func apiCall(){
       // loadingData = false
//        let url  = URL(string: "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(self.locationLat),\(self.locationLong)&radius=9000&type=transit_station&key=AIzaSyBDNQN1SPw803D32ihmOwcEO1IAJzdrupU")
        let url = URL(string:"http://webservices.nextbus.com/service/publicXMLFeed?command=routeList&a=ttc")
        print(url)
        let session = URLSession.shared
        let task = session.dataTask(with: url!) { (data, response, error) in
            if error != nil && data != nil{
                print("error")
            }
            
            guard let response = response as? HTTPURLResponse,(200...299).contains(response.statusCode)else {
                print("Server error")
                return
            }
//            guard let mimetype = response.mimeType,mimetype == "application/json" else{
//                print("Wrong MIME type!")
//                return
//
//            }
            do{
                let json = try JSONSerialization.jsonObject(with: data!, options: [])
                
                
//                self.arrayData2 =  (json as! NSDictionary).value(forKey: "hits") as! NSArray
//                print("second",self.arrayData)
//                self.arrayData.adding(self.arrayData2)
//                print("second12",self.arrayData)
//                print("ArrayData count2",self.arrayData.count)
                DispatchQueue.main.async {
               //     self.tbl_task.reloadData()
                }
                print(json)
            }catch{
                print("JSON error: \(error.localizedDescription)")
                
            }
            
        }
        task.resume()
//        let destLatitude="26.9124"
//        let destLongitude="75.7873"
//        mapView.isMyLocationEnabled = true
//        var urlString = "\("https://maps.googleapis.com/maps/api/directions/json")?origin=\("28.7041"),\("77.1025")&destination=\(destLatitude),\(destLongitude)&sensor=true&key=\("Your-Api-key")"
//
//        urlString = urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)!
//
//        let manager=AFHTTPRequestOperationManager()
//
//        manager.responseSerializer = AFJSONResponseSerializer(readingOptions: JSONSerialization.ReadingOptions.allowFragments) as AFJSONResponseSerializer
//
//        manager.requestSerializer = AFJSONRequestSerializer() as AFJSONRequestSerializer
//
//        manager.responseSerializer.acceptableContentTypes = NSSet(objects:"application/json", "text/html", "text/plain", "text/json", "text/javascript", "audio/wav") as Set<NSObject>
//
//
//        manager.post(urlString, parameters: nil, constructingBodyWith: { (formdata:AFMultipartFormData!) -> Void in
//
//        }, success: {  operation, response -> Void in
//            //{"responseString" : "Success","result" : {"userId" : "4"},"errorCode" : 1}
//            //if(response != nil){
//            let parsedData = JSON(response)
//            print_debug("parsedData : \(parsedData)")
//            var path = GMSPath.init(fromEncodedPath: parsedData["routes"][0]["overview_polyline"]["points"].string!)
//            //GMSPath.fromEncodedPath(parsedData["routes"][0]["overview_polyline"]["points"].string!)
//            var singleLine = GMSPolyline.init(path: path)
//            singleLine.strokeWidth = 7
//            singleLine.strokeColor = UIColor.green
//            singleLine.map = self.mapView
//            //let loginResponeObj=LoginRespone.init(fromJson: parsedData)
//
//
//            //  }
//        }, failure: {  operation, error -> Void in
//
//            print_debug(error)
//            let errorDict = NSMutableDictionary()
//            errorDict.setObject(ErrorCodes.errorCodeFailed.rawValue, forKey: ServiceKeys.keyErrorCode.rawValue as NSCopying)
//            errorDict.setObject(ErrorMessages.errorTryAgain.rawValue, forKey: ServiceKeys.keyErrorMessage.rawValue as NSCopying)
//
//        })
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        
        searchBar.text = ""
        searchBar.resignFirstResponder()
        tbl_nearBy.resignFirstResponder()
        self.searchBar.showsCancelButton = false
        tbl_nearBy.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        
        self.tbl_nearBy.reloadData()
        return true
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchActive = true;
        self.searchBar.showsCancelButton = true
        
        self.arrayFav.removeAll()
       // filtered
        if searchText != ""{
        let filtered = self.title2.filter { $0.contains(searchText) }
//            let filter2 = self.titleTagDict.filter { ((key: "title", value: searchText)) -> Bool in
//                <#code#>
//            }
//            let filteredArrayOnDict = titleTagDict.filter { $1 == searchText}
        self.title1 = filtered
            for (index,element) in filtered.enumerated(){
                if arrSaved.contains(element){
                   self.arrayFav.append(index)
                }
            }
        DispatchQueue.main.async {
self.tbl_nearBy.reloadData()
        }
        }else{
            self.title1 = self.title2
            self.arrayFav = UserDefaults.standard.value(forKey: "SavedFav1") as? [Int] ?? [Int]()

            DispatchQueue.main.async {
                self.tbl_nearBy.reloadData()
            }
        }
//                for  xdata in self.title1
//                {
//                    let nameRange: NSRange = xdata.rangeOfCharacter(from: searchText, options: [NSString.CompareOptions.CaseInsensitiveSearch ,NSString.CompareOptions.AnchoredSearch ])
//
//                    if nameRange.location != NSNotFound{
//
//                        self.filtered.add(xdata)
//                    }
        
                //end of for
                
                
//                self.dispatch_to_main_queue {
//                    /* some code to be executed on the main queue */
//
//                    self.tableView.reloadData()
//
//                } //end of dispatch
        
      //  }
        
        
    }
    
//    func dispatch_to_main_queue(block: dispatch_block_t?) {
//        dispatch_async(dispatch_get_main_queue(), block!)
//    }
//
//    func dispatch_to_background_queue(block: dispatch_block_t?) {
//        let q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
//        dispatch_async(q, block!)
//    }
    

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return title1.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransitTblCell", for: indexPath) as! TransitTblCell
        cell.lbl_title.text = title1[indexPath.row]
        cell.favorite.tag = indexPath.row
        cell.favorite.addTarget(self, action: #selector(favButtonClicked), for: .touchUpInside)
        if arrayFav.contains(indexPath.row){
            let image = UIImage(named: "Favorite")
            cell.favorite.setImage(image, for:.normal)
        }else{
            let image = UIImage(named: "unFave")
            cell.favorite.setImage(image, for:.normal)

        }
        return cell
    }
//   func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete {
////            objects.remove(at: indexPath.row)
////            tableView.deleteRows(at: [indexPath], with: .fade)
//        } else if editingStyle == .insert {
//            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
//        }
//    }
    @objc func favButtonClicked(sender:UIButton){

        if self.arrayFav.contains(sender.tag){
            print(self.arrayFav)
            let indexFromArray = self.arrayFav.firstIndex(of: sender.tag)
            print(indexFromArray)
            self.arrayFav.remove(at:indexFromArray ?? 0)
            arrSaved.remove(at: sender.tag)
        }else{
         self.arrayFav.append(sender.tag)
            arrSaved.append(title1[sender.tag])

        }
      print("Array Fav",self.arrayFav)
//        for (index,element) in arrayFav.enumerated(){
//
//        }
        
        UserDefaults.standard.set(arrSaved, forKey: "SavedFav")
           UserDefaults.standard.set(self.arrayFav, forKey: "SavedFav1")
//        if arrayFav.count == 0{
//            UserDefaults.standard.set("", forKey: "SavedFav")
//            UserDefaults.standard.set([Int](), forKey: "SavedFav1")
//
//        }
        NotificationCenter.default.post(name: Notification.Name("reloadData"), object: nil)

        self.tbl_nearBy.reloadData()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MapViewController") as? MapViewController
        vc?.tagString = self.title1[indexPath.row]
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let editAction = UITableViewRowAction(style: .normal, title: "Set Favorite") { (rowAction, indexPath) in
            //TODO: edit the row at indexPath here
        }
        let newSwiftColor = UIColor(red: 204, green: 204, blue: 0, alpha: 1.0)
        editAction.backgroundColor = .orange
        
//        let deleteAction = UITableViewRowAction(style: .normal, title: "Delete") { (rowAction, indexPath) in
//            //TODO: Delete the row at indexPath here
//        }
//        deleteAction.backgroundColor = .red
        
        return [editAction]
    }
}
